package com.example.goli.sms;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Date;

public class MainActivity extends RuntimePermissionsActivity {
private Button button,write_externul;
private final int read_sms_request_code=80;
private final int write_sms=80;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button=findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.super.requestAppPermissions(new String[]{Manifest.permission.READ_SMS},read_sms_request_code);
            }
        });

    }

    @Override
    public void onPermissionsGranted(int requestCode) {

        if(requestCode==read_sms_request_code){
            Toast.makeText(this, "جواز خواندن اس ام اس داده شود", Toast.LENGTH_SHORT).show();

        }
        else if (requestCode==write_sms){
            Toast.makeText(this, "جواز نوشتن بر روی حافظه", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onPermissionsDeny(int requestCode) {
        Toast.makeText(this, "جواز داده نشد", Toast.LENGTH_SHORT).show();

    }

}
